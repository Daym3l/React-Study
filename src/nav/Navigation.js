import React ,{Component} from 'react';
import {Navbar, NavbarBrand, Nav, NavDropdown, MenuItem} from 'react-bootstrap';


class Navigation extends Component {


    render() {
        return (
            <Navbar collapseOnSelect>
                <Navbar.Header>
                    <NavbarBrand>
                        ESTUDIO-REACT-2018
                    </NavbarBrand>
                </Navbar.Header>
                <Nav pullRight>
                    <NavDropdown title={this.props.title} id="dropdown">
                        <MenuItem>Usuarios Ejemplo</MenuItem>
                    </NavDropdown>
                </Nav>

            </Navbar>
        );
    }
}
export default Navigation;
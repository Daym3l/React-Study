import React, {Component} from 'react';
import {ListGroup, Label} from 'react-bootstrap'
import User from "./User";
import "./User.css"

class UserList extends Component {

    render() {
        return (
            <div>
                <h3><Label>Listado de usuarios</Label></h3>
                <ListGroup>
                    {
                        this.props.users.map(u => {
                            return (
                                <User
                                    key={u.id}
                                    name={u.name}
                                    email={u.email}
                                />
                            )
                        })
                    }
                </ListGroup>
            </div>

        )
    }

}

export default UserList;
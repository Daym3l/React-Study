import React, {Component} from 'react';

import {FormControl, FormGroup, Button, Grid, Row, Col} from 'react-bootstrap';
import './User.css'


class UserForm extends Component {
    render() {
        return (
            <form onSubmit={this.props.onAddUser} className="UserForm">
                <FormGroup >
                    <Grid>
                        <Row className="show-grid">
                            <Col className="separator"  xs={12} md={3} >
                                <FormControl type="text" placeholder="Nombre" name="name"/>
                            </Col>
                            <Col className="separator" xs={12} md={3}>
                                <FormControl  type="text" placeholder="Correo" name="email"/>
                            </Col>
                            <Col xs={8} md={3}>
                                <Button bsStyle="info" type="submit">Insertar</Button>
                            </Col>
                        </Row>
                    </Grid>
                </FormGroup>
            </form>
        );
    }
}

export default UserForm;
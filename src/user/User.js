import React, {Component} from 'react';
import {ListGroupItem} from 'react-bootstrap'


class User extends Component {
    render() {
        return (
            <ListGroupItem>
                {this.props.name} - {this.props.email}
            </ListGroupItem>
        )
    }

}

export default User;
import React, {Component} from 'react';
import {Row, Grid, Col} from 'react-bootstrap'


import UserList from "./UserList";
import UserForm from "./UserForm";

class UserApp extends Component {
    constructor() {
        super();
        this.state = {
            users: [
                {id: 1, name: "Daymel", email: "daymel@xetid.cu"},
                {id: 2, name: "Ibelise", email: "ibe@copextel.cu"}
            ]
        }

    }

    handleOnAdduser(event) {
        event.preventDefault();
        let user = {
            name: event.target.name.value,
            email: event.target.email.value
        };
        this.setState({
            users: this.state.users.concat([user])
        })
    }

    render() {
        return (
            <Grid>
                <Row className="show-grid">
                    <UserForm onAddUser={this.handleOnAdduser.bind(this)}/>
                </Row>
                <Row className="show-grid">
                    <Col lg={12}>
                        <UserList users={this.state.users}/>
                    </Col>
                </Row>


            </Grid>

        );
    }

}

export default UserApp;